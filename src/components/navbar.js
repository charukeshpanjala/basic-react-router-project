import { Link } from "react-router-dom";
import "./index.css"

const Navbar = () => {
    return ( <nav className="navbar">
        <Link to="/" className="link-element">Home</Link>
        <Link to="/about" className="link-element">About</Link>
        <Link to="/contactUs" className="link-element">Contact Us</Link>
    </nav>);
}
 
export default Navbar;