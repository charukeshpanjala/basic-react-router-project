import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './components/home.js';
import About from './components/about.js';
import ContactUs from './components/contactUs.js'
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/contactus" element={<ContactUs />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
