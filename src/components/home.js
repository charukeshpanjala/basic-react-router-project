import Navbar from './navbar.js'
import "./index.css";

const Home = () => {
    return (
        <div className="home-container">
            <Navbar />
            <h1 className="home-heading">"Hi, there !"</h1>
            <p className="home-description">I am Charukesh Panjala</p>
        </div>);
}
export default Home;