import React, { Component } from 'react';
import Navbar from './navbar.js'
import "./index.css";

class ContactUs extends Component {
    state = { name: "", ph: "", showNameError: false, showPhoneError: false, showSuccess: false };

    onFormSubmit = (event) => {
        event.preventDefault();
        if (this.state.name === "") {
            this.setState({ showNameError: true })
        }
    }

    onchangeName = (event) => {
        this.setState({ name: event.target.value })
    }
    onchangePhone = (event) => {
        this.setState({ ph: event.target.value })
    }

    onFormSubmit = (event) => {
        event.preventDefault();
        if (this.state.name === "" && (this.state.ph === "" || this.state.ph.length !== 10)) {
            this.setState({ showNameError: true, showPhoneError: true });
        }
        if (this.state.name === "" && this.state.ph.length === 10) {
            this.setState({ showNameError: true, showPhoneError: false });
        }
        if ((this.state.ph === "" || this.state.ph.length !== 10) && this.state.name !== "") {
            this.setState({ showPhoneError: true, showNameError: false });
        }
        if (this.state.name !== "" && this.state.ph.length === 10) {
            this.setState({ name: "", ph: "", showSuccess: true, showNameError: false, showPhoneError: false });
        }
    }


    render() {
        return (
            <div className="contact-main-container">
                <Navbar />
                <div className="about-container">
                    <div className="map-container"><iframe className="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.6022063509618!2d77.60984591468522!3d12.933269290881023!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae14526d47175f%3A0x8a51177fba6ca896!2s91springboard%207th%20Block%2C%20Koramangala!5e0!3m2!1sen!2sin!4v1651263238739!5m2!1sen!2sin"></iframe>
                    </div>
                    <form className="contact-form" onSubmit={this.onFormSubmit}>
                        <label htmlFor="name" className="label">Name</label>
                        <input id="name" type="text" className="input" value={this.state.name} placeholder="Enter your Name" onChange={this.onchangeName} />
                        {this.state.showNameError && <p className="error">*Enter valid name</p>}
                        <label htmlFor="phone" className="label">Phone Number</label>
                        <input id="phone" type="text" className="input" value={this.state.ph} placeholder="Enter your Phone Number" onChange={this.onchangePhone} />
                        {this.state.showPhoneError && <p className="error">*Enter valid phone number</p>}
                        <button className="button" type="submit" >Submit</button>
                        {this.state.showSuccess && <p className="success">Sucessfully Submitted</p>}
                    </form>
                </div>
            </div>
        )
    }
}

export default ContactUs;