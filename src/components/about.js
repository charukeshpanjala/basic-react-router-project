import Navbar from './navbar.js'
import pic from "../profilepic.jpeg"
import "./index.css";

const About = () => {
    return (
        <div className="about-main-container">
            <Navbar />
            <div className="about-container">
                <div className="description-container">
                    <h1 className="about-description">About</h1>
                    <p className="about-description">Designation- Software Developer</p>
                </div>
                <div className="img-container">
                    <img className="profile-img" src={pic} />
                </div>
            </div>
        </div>);
}
export default About;